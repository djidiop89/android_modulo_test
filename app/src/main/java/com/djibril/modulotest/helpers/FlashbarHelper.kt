/*
 * Copyright (c) 2020 by Djibril, Inc.
 * All Rights Reserved
 */

package com.djibril.modulotest.helpers

import com.andrognito.flashbar.Flashbar
import com.andrognito.flashbar.anim.FlashAnim
import com.djibril.modulotest.R
import com.djibril.modulotest.core.BaseActivity

class FlashbarHelper {

    companion object {
        @JvmStatic
        fun createFlashBarError(activity: BaseActivity, title: String, message: String): Flashbar {
            return Flashbar.Builder(activity)
                .gravity(Flashbar.Gravity.TOP)
                .title(title)
                .message(message)
                .messageSizeInSp(12f)
                .primaryActionText(R.string.flashbar_error_retry)
                .primaryActionTapListener(object : Flashbar.OnActionTapListener {
                    override fun onActionTapped(bar: Flashbar) {
                        activity.finish()
                        activity.startActivity(activity.intent)
                    }
                })
                .backgroundColorRes(R.color.mango)
                .enterAnimation(
                    FlashAnim.with(activity)
                        .animateBar()
                        .duration(750)
                        .alpha()
                        .overshoot()
                )
                .exitAnimation(
                    FlashAnim.with(activity)
                        .animateBar()
                        .duration(400)
                        .accelerateDecelerate()
                )
                .build()
        }

    }
}