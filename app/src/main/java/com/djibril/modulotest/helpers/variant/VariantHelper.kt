/*
 * Copyright (c) 2020 by Djibril, Inc.
 * All Rights Reserved
 */

package com.djibril.modulotest.helpers.variant

interface VariantHelper {

    fun getBackendEndPoint(): String
}