/*
 * Copyright (c) 2020 by Djibril, Inc.
 * All Rights Reserved
 */

package com.djibril.modulotest.ui.shared

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.djibril.modulotest.R
import com.djibril.modulotest.core.BaseActivity


class ProgressDialogFragment :androidx.fragment.app.DialogFragment() {
    private lateinit var mContext: Context

    companion object {
        @JvmStatic
        fun createDialog(): ProgressDialogFragment {
            return ProgressDialogFragment()
        }
    }

    fun show(activity: BaseActivity, tag: String) {
        super.show(activity.supportFragmentManager, tag)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.ProgressDialogStyle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        dialog!!.window!!.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
        return inflater.inflate(R.layout.dialog_progress, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.mContext = requireContext()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isCancelable = false
    }

}
