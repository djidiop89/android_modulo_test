package com.djibril.modulotest.di

import com.djibril.modulotest.App
import dagger.Component


@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun inject(application: App)
}