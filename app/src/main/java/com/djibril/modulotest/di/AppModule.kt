/*
 * Copyright (c) 2020 by Djibril, Inc.
 * All Rights Reserved
 */

package com.djibril.modulotest.di

import android.content.Context
import com.djibril.modulotest.App
import com.djibril.modulotest.AppLifecycleObserver
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val application: App) {

    @Provides
    fun provideApplicationContext(): Context = application

    @Provides
    fun provideAppLifecycleObserver(): AppLifecycleObserver = AppLifecycleObserver(application)


}