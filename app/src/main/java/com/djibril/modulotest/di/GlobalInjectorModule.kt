/*
 * Copyright (c) 2020 by Djibril, Inc.
 * All Rights Reserved
 */

package com.djibril.modulotest.di

import android.app.Activity
import android.content.Context
import com.djibril.modulotest.App
import com.djibril.modulotest.helpers.network.NetworkConnectionInterceptor
import com.djibril.modulotest.helpers.variant.VariantHelper
import com.djibril.modulotest.helpers.variant.VariantHelperImpl
import com.orhanobut.logger.Logger
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import org.jetbrains.anko.runOnUiThread
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit


@Module
class GlobalInjectorModule(var activity: Activity) {

    @Provides
    fun provideContext(): Context = activity

    @Provides
    fun provideApplication(): App = activity.application as App

    @Provides
    fun provideVariantHelper(helper: VariantHelperImpl): VariantHelper = helper


   /*
    //API
    @Provides
    fun provideAuthApiService(service: AuthApiServiceImpl): AuthApiService = service*/



    @Provides
    fun provideRetrofit(): Retrofit {
        val moshi = Moshi.Builder()
            .build()
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
            .client(provideOkHttpClient())
            .baseUrl(provideVariantHelper(VariantHelperImpl()).getBackendEndPoint())
            .build()
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val okhttpClientBuilder = OkHttpClient.Builder()
        okhttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS)
        okhttpClientBuilder.readTimeout(30, TimeUnit.SECONDS)
        okhttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS)
        //todo make better
        okhttpClientBuilder.addInterceptor { chain ->
            val request = chain.request().newBuilder().build()
            var response = chain.proceed(request)
            var tryCount = 0
            if (response.code() == 401 || response.code() == 403) {
                provideContext().runOnUiThread {
                    provideApplication().onUnauthorized()
                }
            } else {
                while (response.code() == 429 && tryCount < 3) {
                    Logger.d("429 : Request is not successful - $tryCount")
                    tryCount++
                    response = chain.proceed(request)
                }
            }

            response
        }


        okhttpClientBuilder.addInterceptor(object : NetworkConnectionInterceptor() {
            override fun isInternetAvailable(): Boolean {
                return App.isDeviceConnected
            }

            override fun onInternetUnavailable() {
                provideContext().runOnUiThread {
                    provideApplication().onInternetUnavailable()
                }
            }
        })

        return okhttpClientBuilder.build()

    }

}