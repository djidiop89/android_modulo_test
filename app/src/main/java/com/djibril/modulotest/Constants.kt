/*
 * Copyright (c) 2020 by Djibril, Inc.
 * All Rights Reserved
 */

package com.djibril.modulotest

import java.util.*

class Constants {

    companion object {
        const val DEV_URL = "http://monliv.seedofafrica.com/"
        const val PROD_URL = "http://monliv.seedofafrica.com/"
    }

    enum class HTTPStatus constructor(val code: Int) {
        BAD_REQUEST(400),
        UNAUTHORIZED(401),
        FORBIDDEN(403),
        BAD_EMAIL(422),
        NOT_FOUND(404),
        INTERNAL_SERVER_ERROR(500);

        companion object {
            fun from(findValue: Int): HTTPStatus =
                HTTPStatus.values().first { it.code == findValue }
        }
    }

}