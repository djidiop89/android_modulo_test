/*
 * Copyright (c) 2020 by Djibril, Inc.
 * All Rights Reserved
 */
package com.djibril.modulotest

import android.app.Application
import androidx.lifecycle.ProcessLifecycleOwner
import android.content.IntentFilter
import android.net.ConnectivityManager
import com.djibril.modulotest.di.AppModule
import com.djibril.modulotest.di.DaggerAppComponent
import com.djibril.modulotest.helpers.network.ConnectivityReceiver
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import javax.inject.Inject


class App : Application(), ConnectivityReceiver.ConnectivityReceiverListener,
    AppLifecycleObserver.AppLifecycleListener, ApiErrorListener {

    @Inject
    lateinit var appLifecycleObserver: AppLifecycleObserver


    private var apiErrorListener: ApiErrorListener? = null


    companion object {
        var isDeviceConnected = false
        var isDeviceBackground = false
    }

    override fun onCreate() {
        super.onCreate()
       Logger.addLogAdapter(AndroidLogAdapter())

       // Fabric.with(this,Crashlytics())

        DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
            .inject(this)


        ProcessLifecycleOwner.get().lifecycle.addObserver(appLifecycleObserver)
        ConnectivityReceiver.connectivityReceiverListener = this


    }

    fun setInternetConnectionListener(listener: ApiErrorListener?) {
        apiErrorListener = listener
    }

    fun hasInternetConnectionListener(): Boolean {
        return apiErrorListener != null
    }

    fun removeApiErrorListner() {
        apiErrorListener = null
    }

    override fun onUnauthorized() {
        apiErrorListener?.onUnauthorized()
    }

    override fun onInternetUnavailable() {
        apiErrorListener?.onInternetUnavailable()
    }

    override fun onInternetAvailable() {
        apiErrorListener?.onInternetAvailable()
    }

    override fun onEnteringForeground() {
        isDeviceBackground = false
        observeConnectivity()
    }

    override fun onEnteringBackGround() {
        isDeviceBackground = true
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        isDeviceConnected = isConnected
    }

    private fun observeConnectivity() {
        registerReceiver(ConnectivityReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }
}

interface ApiErrorListener {
    fun onInternetUnavailable()
    fun onInternetAvailable()
    fun onUnauthorized()
}